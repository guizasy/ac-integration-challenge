
package com.avenuecode.challenge.integration;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.avenuecode.challenge.integration package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.avenuecode.challenge.integration
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetBonusResponse }
     * 
     */
    public GetBonusResponse createGetBonusResponse() {
        return new GetBonusResponse();
    }

    /**
     * Create an instance of {@link GetAverageSalaryResponse }
     * 
     */
    public GetAverageSalaryResponse createGetAverageSalaryResponse() {
        return new GetAverageSalaryResponse();
    }

    /**
     * Create an instance of {@link GetAverageSalary }
     * 
     */
    public GetAverageSalary createGetAverageSalary() {
        return new GetAverageSalary();
    }

    /**
     * Create an instance of {@link GetBonus }
     * 
     */
    public GetBonus createGetBonus() {
        return new GetBonus();
    }

}
