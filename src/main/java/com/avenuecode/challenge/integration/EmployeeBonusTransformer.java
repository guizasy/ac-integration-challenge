package com.avenuecode.challenge.integration;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractTransformer;
import org.mule.util.CaseInsensitiveHashMap;

@SuppressWarnings("unchecked")
public class EmployeeBonusTransformer extends AbstractTransformer {

	@Override
	protected Object doTransform(Object src, String enc)
			throws TransformerException {

		BigDecimal bonus = null;
		Random rand = new Random();

		LinkedList<Object> list = (LinkedList<Object>) src;
		CaseInsensitiveHashMap caseInsHashMap = (CaseInsensitiveHashMap) list.getFirst();
		Optional<Map.Entry<String, BigDecimal>> optional = caseInsHashMap.entrySet().stream().findFirst();
		BigDecimal salary = optional.get().getValue();
		BigDecimal percentage = new BigDecimal (rand.nextInt(50) / 100.0);
		bonus = salary.multiply(percentage);

		return bonus.setScale(2, RoundingMode.HALF_EVEN);
	}
}

