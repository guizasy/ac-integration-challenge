%dw 1.0
%output application/json
---
payload map ((payload01, index) -> {
	id: payload01.ID,
	firstName: payload01.FIRST_NAME,
	lastName: payload01.LAST_NAME,
	salary: payload01.SALARY
})